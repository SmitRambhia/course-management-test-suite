package factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class DriverFactory {
    public static ThreadLocal<WebDriver> tl = new ThreadLocal<>();
    public ChromeOptions chromeOptions;

    public void setDriver(){
        System.setProperty("webdriver.chrome.driver","src/test/resources/drivers/chromedriver.exe");
        chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--remote-allow-origins=*");
        tl.set(new ChromeDriver(chromeOptions));
        getDriver().manage().window().maximize();
    }

    public static WebDriver getDriver(){
        return tl.get();
    }
}
