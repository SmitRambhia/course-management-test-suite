package seeder;

import java.util.concurrent.ThreadLocalRandom;

public class EnrollmentFormSeeder{
    public String getRandomStream(){
        String[] streams = {"Diploma","Degree"};
        return streams[ThreadLocalRandom.current().nextInt(0, streams.length)];
    }

    public String getRandomDiplomaSemester(){
        int randomSemester = ThreadLocalRandom.current().nextInt(1,7);
        return "Semester "+randomSemester;
    }

    public String getRandomDegreeSemester(){
        int randomSemester = ThreadLocalRandom.current().nextInt(1,9);
        return "Semester "+randomSemester;
    }

    public String getRandomInstitute(){
        String[] institutes = {"Bharati Vidyapeeth College of Engineering, Belapur, Navi Mumbai","Datta Meghe College of Engineering, Airoli, Navi Mumbai"};
        return institutes[ThreadLocalRandom.current().nextInt(0,institutes.length)];
    }

    public String getRandomBranch(){
        String[] branches  ={"Computer Engineering","Information Technology"};
        return branches[ThreadLocalRandom.current().nextInt(0,branches.length)];

    }


}