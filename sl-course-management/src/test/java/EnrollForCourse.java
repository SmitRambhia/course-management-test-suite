import com.github.javafaker.Faker;
import factory.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import seeder.EnrollmentFormSeeder;
import java.time.Duration;
import java.util.HashMap;

public class EnrollForCourse extends BaseTest {
    Faker faker;
    EnrollmentFormSeeder enrollmentFormSeeder;

    @BeforeClass
    public void setUp(){
        faker = new Faker();
        enrollmentFormSeeder = new EnrollmentFormSeeder();
    }

    @Test
    public void enrollForCourse(ITestContext ctx) throws InterruptedException{
        WebDriver driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        HashMap<String,Object> enrollmentFormData = new HashMap<>();

        driver.get("https://staging.studylinkclasses.com/courses");

        WebElement courseCard = driver.findElement(By.cssSelector(".education_block_grid"));
        String singleCourseURL = courseCard.findElement(By.cssSelector(".education_block_body h4.bl-title a")).getText().toLowerCase().replaceAll(" ","-").replace(".","");

        driver.get("https://staging.studylinkclasses.com/courses/"+singleCourseURL);

        driver.findElement(By.xpath("//a[@class='btn btn-block btn-theme course-enquiry-button']")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("enquiryModal")));

        String fakeFirstName = faker.name().firstName();
        driver.findElement(By.xpath("//form[@id='enquiryForm']//input[@name='first_name']")).sendKeys(fakeFirstName);
        enrollmentFormData.put("firstName",fakeFirstName);

        String fakeLastName = faker.name().lastName();
        driver.findElement(By.xpath("//form[@id='enquiryForm']//input[@name='last_name']")).sendKeys(fakeLastName);
        enrollmentFormData.put("lastName",fakeLastName);

        String fakeEmail = fakeFirstName+fakeLastName+"@gmail.com";
        driver.findElement(By.xpath("//form[@id='enquiryForm']//input[@name='email']")).sendKeys(fakeEmail);
        enrollmentFormData.put("email",fakeEmail);

        String fakePhone = faker.number().digits(10);
        driver.findElement(By.xpath("//form[@id='enquiryForm']//input[@name='phone_number']")).sendKeys(fakePhone);
        enrollmentFormData.put("phoneNumber",fakePhone);

        driver.findElement(By.cssSelector("#stream-dropdown-parent .select2-container")).click();
        String randomStream = enrollmentFormSeeder.getRandomStream();
        driver.findElement(By.className("select2-search__field")).sendKeys(randomStream,Keys.ENTER);
        enrollmentFormData.put("stream",randomStream);

        Thread.sleep(3000);

        driver.findElement(By.cssSelector("#semester-dropdown-parent .select2-container")).click();
        String randomSemester = "";
        if(randomStream.equals("Diploma")){
            randomSemester = enrollmentFormSeeder.getRandomDiplomaSemester();
        }
        else{
            randomSemester = enrollmentFormSeeder.getRandomDegreeSemester();
        }
        driver.findElement(By.className("select2-search__field")).sendKeys(randomSemester,Keys.ENTER);
        enrollmentFormData.put("semester",randomSemester);

        driver.findElement(By.cssSelector("#institute-dropdown-parent .select2-container")).click();
        String randomInstitute = enrollmentFormSeeder.getRandomInstitute();
        driver.findElement(By.className("select2-search__field")).sendKeys(randomInstitute,Keys.ENTER);
        enrollmentFormData.put("institute",randomInstitute);

        driver.findElement(By.cssSelector("#branch-dropdown-parent .select2-container")).click();
        String randomBranch = enrollmentFormSeeder.getRandomBranch();
        driver.findElement(By.className("select2-search__field")).sendKeys(randomBranch,Keys.ENTER);
        enrollmentFormData.put("branch",randomBranch);

        String randomMessage = faker.lorem().paragraph(2);
        driver.findElement(By.id("message")).sendKeys(randomMessage);
        enrollmentFormData.put("message",randomMessage);

        driver.findElement(By.xpath("//button[normalize-space()='Approach Us']")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='msgSubmit'][normalize-space()='Message Submitted!']")));





    }

}
