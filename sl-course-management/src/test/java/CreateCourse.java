import com.github.javafaker.Faker;
import factory.DriverFactory;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import utils.DateUtils;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.HashMap;

public class CreateCourse extends  BaseTest {

    private Faker faker;

    @BeforeTest
    public void setup(){
        faker = new Faker();
    }

    public void login(WebDriver driver, String username, String password){
        driver.get("https://staging.studylinkclasses.com/login");
        driver.findElement(By.name("email")).sendKeys(username);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("kt_login_signin_submit")).click();
    }

    public void goToCourseSection(WebDriver driver, Actions actions){
        WebElement sidebar = driver.findElement(By.id("kt_aside"));
        actions.moveToElement(sidebar);
        actions.perform();

        WebElement sideBarCoursesLink = driver.findElement(By.xpath("//a[@href='https://staging.studylinkclasses.com/control-panel/courses']"));
        actions.moveToElement(sideBarCoursesLink);
        actions.perform();
        sideBarCoursesLink.click();

        boolean isManageCoursesVisible = driver.findElement(By.xpath("//h3[normalize-space()='Manage Courses']")).isDisplayed();
        Assert.assertTrue(isManageCoursesVisible,"Redirected to wrong page after clicking on courses sidebar link");
    }

    @Test
    public void loginWithAdminAndCreateCourse(ITestContext ctx) throws InterruptedException {
        WebDriver driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        Actions actions = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor)driver;

        HashMap<String,Object> createCourseData = new HashMap<>();
        login(driver,"admin34@studylinkclasses.com","abcd1234");
        boolean isTitleVisible = driver.findElement(By.xpath("//h3[normalize-space()='Study Link -Admin Panel']")).isDisplayed();
        Assert.assertTrue(isTitleVisible,"Redirected to wrong page or login not working");

        goToCourseSection(driver,actions);

        driver.findElement(By.xpath("//a[@href='https://staging.studylinkclasses.com/control-panel/courses/create']")).click();

        String fakeCourseName = faker.lorem().sentence(3);
        driver.findElement(By.name("name")).sendKeys(fakeCourseName);
        createCourseData.put("name",fakeCourseName);

        String fakeCourseStatus = faker.lorem().sentence(2);
        driver.findElement(By.name("status")).sendKeys(fakeCourseStatus);
        createCourseData.put("status",fakeCourseStatus);

        String fakeDuration = Integer.toString(faker.number().numberBetween(1,10));
        driver.findElement(By.name("duration_in_weeks")).sendKeys(fakeDuration+"w");
        createCourseData.put("duration",fakeDuration+"w");

        String fakeNumberOfHours = Integer.toString(faker.number().numberBetween(10,50));
        driver.findElement(By.name("number_of_hours")).sendKeys(fakeNumberOfHours);
        createCourseData.put("hours",fakeNumberOfHours);

        String fakeNumberOfProjects = Integer.toString(faker.number().numberBetween(1,5));
        driver.findElement(By.name("number_of_projects")).sendKeys(fakeNumberOfProjects);
        createCourseData.put("numberOfProjects",fakeNumberOfProjects);

        driver.findElement(By.id("select2-kt_select2_2-container")).click();
        driver.findElement(By.xpath("//input[@role='searchbox']")).sendKeys("Yes", Keys.ENTER);
        createCourseData.put("isCertificateProvided","Yes");

        driver.findElement(By.id("select2-kt_select2_3-container")).click();
        driver.findElement(By.xpath("//input[@role='searchbox']")).sendKeys("Yes",Keys.ENTER);

        driver.findElement(By.name("start_date")).click();
        driver.findElement(By.cssSelector(".today.day")).click();
        LocalDateTime todaysDate = LocalDateTime.now();
        createCourseData.put("startDate",todaysDate);

        File file = new File("src/test/resources/images/courses/automation-testing.jpg");
        String courseImageFilePath = file.getAbsolutePath();
        driver.findElement(By.name("image")).sendKeys(courseImageFilePath);

        String selectedStream = "Diploma Engineering";
        driver.findElement(By.xpath("//input[@class='dual-listbox__search']")).sendKeys(selectedStream);
        driver.findElement(By.xpath("//ul[@class='dual-listbox__available']//li[@class='dual-listbox__item'][normalize-space()='Diploma Engineering']")).click();
        driver.findElement(By.xpath("//button[@class='dual-listbox__button'][normalize-space()='Add']")).click();
        String extractedSelectedStream = driver.findElement(By.xpath("//ul[@class='dual-listbox__selected']/li[1]")).getText().trim();
        Assert.assertEquals(extractedSelectedStream,selectedStream,"Dual List Component of Select Stream input is not working");
        createCourseData.put("selectedStream",selectedStream);

        String fakeCourseDescription = faker.lorem().paragraph();
        driver.switchTo().frame("course-body_ifr");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body")));
        WebElement body = driver.findElement(By.cssSelector("body"));
        js.executeScript("arguments[0].innerHTML='"+fakeCourseDescription+"'",body);
        driver.switchTo().parentFrame();
        createCourseData.put("description",fakeCourseDescription);

        int numberOfParts = faker.number().numberBetween(1,5);
        //int numberOfParts = 3;
        List<String> parts = new ArrayList<>();
        String partID = "part_name_";
        WebElement addPartButton = driver.findElement(By.id("add_part"));

        for(int i =0;i<numberOfParts;i++){
            if(i!=0){
                actions.moveToElement(addPartButton);
                wait.until(ExpectedConditions.elementToBeClickable(addPartButton));
                addPartButton.click();
            }

            String fakePart = faker.lorem().sentence(5);
            driver.findElement(By.id(partID+(i+1))).sendKeys(fakePart);
            driver.findElement(By.xpath("//div[@id='topic_row_"+(i+1)+"']//button")).click();
            parts.add(fakePart);
        }
        createCourseData.put("parts",parts);
        System.out.println(parts+"\n");

        Thread.sleep(1000);

        driver.findElement(By.xpath("//div[@id='review_row_1']//button")).click();
        driver.findElement(By.xpath("//button[normalize-space()='Submit']")).click();

        ctx.setAttribute("createdCourseData",createCourseData);
    }

    @Test(dependsOnMethods = {"loginWithAdminAndCreateCourse"})
    public void searchAndValidateCreatedCourse(ITestContext ctx) throws InterruptedException {
        WebDriver driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        Actions actions = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        HashMap<String, Object> createCourseData = (HashMap<String, Object>) ctx.getAttribute("createdCourseData");
        login(driver,"admin34@studylinkclasses.com","abcd1234");
        goToCourseSection(driver,actions);
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)createCourseData.get("name"));

        Thread.sleep(5000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']")));
        String datatableCourseName = driver.findElement(By.xpath("//table[@id='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']")).getText();
        Assert.assertEquals(datatableCourseName,createCourseData.get("name"));

        String datatableCourseStatus = driver.findElement(By.xpath("//table[@id='courses_datatable']/tbody/tr//td[4][normalize-space()='"+createCourseData.get("status")+"']")).getText();
        Assert.assertEquals(datatableCourseStatus,createCourseData.get("status"));

        String formattedActualDate = DateUtils.getFormattedDate("dd MMM, yyyy",(LocalDateTime)createCourseData.get("startDate"));
        Assert.assertEquals(driver.findElement(By.xpath("//table[@id='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[5]")).getText(),formattedActualDate);

        String datatableDuration = driver.findElement(By.xpath("//table[@id='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[6]")).getText();
        Assert.assertEquals(datatableDuration,createCourseData.get("duration"));

        Assert.assertEquals("0",driver.findElement(By.xpath("//table[@id ='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[7]")).getText());

        String datatableNumberOfHours = driver.findElement(By.xpath("//table[@id ='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[8]")).getText();
        Assert.assertEquals(datatableNumberOfHours,createCourseData.get("hours"));

        String datatableProject = driver.findElement(By.xpath("//table[@id ='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[9]")).getText();
        Assert.assertEquals(datatableProject,createCourseData.get("numberOfProjects"));

        String datatableCertificateStatus = driver.findElement(By.xpath("//table[@id ='courses_datatable']/tbody/tr//td[3][normalize-space()='"+createCourseData.get("name")+"']/parent::tr/td[10]")).getText();
        Assert.assertEquals(datatableCertificateStatus,createCourseData.get("isCertificateProvided"));
    }

    @Test(dependsOnMethods = {"loginWithAdminAndCreateCourse"})
    public void visitPublicCoursePageAndValidateCreatedCourse(ITestContext ctx){
        WebDriver driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        Actions actions = new Actions(driver);
        JavascriptExecutor js = (JavascriptExecutor)driver;
        HashMap<String, Object> createdCourseData = (HashMap<String, Object>) ctx.getAttribute("createdCourseData");

        driver.get("https://staging.studylinkclasses.com/courses");
        driver.findElement(By.xpath("//input[@placeholder='Search Courses']")).sendKeys((String)createdCourseData.get("name"), Keys.ENTER);
        WebElement courseCard = driver.findElement(By.xpath("//div[contains(@class,'education_block_grid')]/div[@class='education_block_body'][normalize-space()='"+createdCourseData.get("name")+"']/parent::div"));

        String extractedCourseName = courseCard.findElement(By.cssSelector(".education_block_body h4.bl-title a")).getText().toLowerCase();
        Assert.assertEquals(extractedCourseName,((String) createdCourseData.get("name")).toLowerCase());

        String extractedCourseStartDate = courseCard.findElement(By.cssSelector(".courses_info_style2>ul>li:nth-child(1)")).getText();
        Assert.assertEquals(extractedCourseStartDate, DateUtils.getFormattedDate("dd MMM, yyyy",(LocalDateTime) createdCourseData.get("startDate")));
    }

    @Test(dependsOnMethods = {"loginWithAdminAndCreateCourse"})
    public void openDetailedViewAndValidateCreatedCourse(ITestContext ctx){
        WebDriver driver = DriverFactory.getDriver();
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        JavascriptExecutor js = (JavascriptExecutor)driver;
        HashMap<String, Object> createdCourseData = (HashMap<String, Object>) ctx.getAttribute("createdCourseData");
        String courseDetailedViewURL = "https://staging.studylinkclasses.com/courses/"+createdCourseData.get("name").toString().trim().replace(".","").replaceAll(" ","-").toLowerCase();
        driver.get(courseDetailedViewURL);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ed_detail_wrap")));

        WebElement courseDetailsList = driver.findElement(By.cssSelector(".ed_detail_wrap"));

        String detailedViewDuration = courseDetailsList.findElement(By.cssSelector(".ed_detail_wrap ul>li:nth-child(1)>strong")).getText();
        Assert.assertEquals(detailedViewDuration,createdCourseData.get("hours"));

        String detailedViewStartDate = courseDetailsList.findElement(By.cssSelector(".ed_detail_wrap ul>li:nth-child(2)>strong")).getText();
        Assert.assertEquals(detailedViewStartDate,DateUtils.getFormattedDate("dd MMM, yyyy",(LocalDateTime)createdCourseData.get("startDate")));

        String detailedViewNumberOfProjects = courseDetailsList.findElement(By.cssSelector(".ed_detail_wrap ul>li:nth-child(3)>strong")).getText();
        Assert.assertEquals(detailedViewNumberOfProjects,createdCourseData.get("numberOfProjects"));

        String detailedViewCertificateStatus = courseDetailsList.findElement(By.cssSelector(".ed_detail_wrap ul>li:nth-child(4)>strong")).getText();
        Assert.assertEquals(detailedViewCertificateStatus,createdCourseData.get("isCertificateProvided"));

        List<WebElement> extractedCurriculumParts = driver.findElements(By.cssSelector("#accordionExample .card h6"));
        List<String> actualCurriculumParts = (List<String>) createdCourseData.get("parts");
        System.out.println(extractedCurriculumParts.size());
        System.out.println(actualCurriculumParts.size());
        for(int i=0;i<extractedCurriculumParts.size();i++){
            String extractedPart = extractedCurriculumParts.get(i).getText().split(":")[1].trim().toLowerCase();
            //System.out.println("Expected: "+extractedPart+"\n");
            String actualPart = actualCurriculumParts.get(i).toLowerCase();
            //System.out.printf("Actual: "+actualPart+"\n");
            //Assert.assertEquals(extractedPart,actualPart);

        }
    }

}
