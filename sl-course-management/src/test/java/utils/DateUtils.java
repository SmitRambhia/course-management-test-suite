package utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {
    public static String getFormattedDate(String dateFormat, LocalDateTime dateTime){
        LocalDateTime localDateObj = dateTime;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateFormat);
        return localDateObj.format(formatter);
    }
}
