import factory.DriverFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    DriverFactory driverFactory = new DriverFactory();

    @BeforeMethod
    public void setDriver(){
        driverFactory.setDriver();
    }

    @AfterMethod
    public void tearDown(){
        DriverFactory.getDriver().quit();
    }
}
